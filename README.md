#  A validator for the Open Know How Standard

This is a validator for the Open Know How Standard manifest files.

Live at: https://buildinstructions.org/okh-validator/

http://openknowhow.org

## Don't know javascript?

You can still help by improving the data that the validator uses:

- [required_fields.yaml](required_fields.yaml)
- [recommended_fields.yaml](recommended_fields.yaml)
- [remaining_fields.yaml](recommended_fields.yaml)
- [example_okh.yaml](example_okh.yaml)

## Project setup

Install [NodeJS](https://nodejs.org/en/download/) (version 8 or higher) then:

```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
