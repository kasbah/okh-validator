module.exports = {
  outputDir: "public/okh-validator",
  publicPath: "/okh-validator/",
  configureWebpack: config => {
    config.module.rules.push({
      test: /\.yaml|\.yml/i,
      use: "raw-loader"
    });
  }
};
